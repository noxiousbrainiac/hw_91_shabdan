const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const Albums = require('../models/Albums');
const {nanoid} = require("nanoid");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const User = require("../models/User");
const {ValidationError} = require('mongoose').Error;

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const token = req.get('Authorization');

        if (!token) {
            if (req.query.artist) {
                const album = await Albums.find({artist: req.query.artist, published: true})
                    .populate('artist', 'title')
                    .sort({productionYear: +1});
                return res.send(album);
            }
            const albums = await Albums.find();
            res.send(albums);
        }

        const user = await User.findOne({token});

        if (user.role !== 'admin') {
            if (req.query.artist) {
                const album = await Albums.find({artist: req.query.artist, published: true})
                    .populate('artist', 'title')
                    .sort({productionYear: +1});
                return res.send(album);
            }
            const albums = await Albums.find();
            res.send(albums);
        }

        if (req.query.artist) {
            const album = await Albums.find({artist: req.query.artist})
                .populate('artist', 'title')
                .sort({productionYear: +1});
            return res.send(album);
        }
        const albums = await Albums.find();
        res.send(albums);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album = await Albums.findById(req.params.id);

        if (album) {
            return res.send(album);
        } else {
            return res.status(404).send({error: "Album not found"});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', auth , permit('admin'), upload.single('image'), async (req, res) => {
    try {
        const albumData = {
            title: req.body.title,
            artist: req.body.artist,
            productionYear: req.body.productionYear
        }

        if (req.file) {
            albumData.image = 'uploads/' + req.file.filename;
        }

        const album = new Albums(albumData);
        await album.save();
        res.send(album);
    } catch (e) {
        if(e instanceof ValidationError) {
            return res.status(400).send(e);
        }
        res.status(500).send(e);
    }
});

router.post('/:id/publish', auth, permit('admin'),async (req, res) => {
    try {
        const albums = await Albums.findById(req.params.id);

        if (albums) {
            albums.published = !albums.published;
            await albums.save();
            res.send(albums);
        } else {
            res.status(404).send({message: 'Not found'});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id',  auth, permit('admin'),async(req, res) => {
    try {
        const artist = await Albums.findByIdAndDelete(req.params.id);

        if (artist) {
            res.send(`Album by ${req.params.id} was deleted successful`);
        } else {
            res.status(404).send({message: 'Album not found'});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;

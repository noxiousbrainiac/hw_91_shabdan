const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const Artists = require('../models/Artists');
const {nanoid} = require('nanoid');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const User = require('../models/User');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const token = req.get('Authorization');

        if (!token) {
            const artists = await Artists.find({published: true});
            return res.send(artists);
        }

        const user = await User.findOne({token});

        if (user.role === 'admin') {
            const artists = await Artists.find();
            return res.send(artists);
        }

        const artists = await Artists.find({published: true});
        return res.send(artists);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', auth , permit('admin'), upload.single('image'), async (req, res) => {
    try {
        const artistData = {
            title: req.body.title,
        };

        if (req.file) {
            artistData.image = 'uploads/' + req.file.filename;
        }

        if (req.body.info) {
            artistData.info = req.body.info;
        }

        const artist = new Artists(artistData);
        await artist.save();
        res.send(artist);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/:id/publish', auth, permit('admin'),async (req, res) => {
   try {
       const artist = await Artists.findById(req.params.id);

       if (artist) {
           artist.published = !artist.published;
           await artist.save();
           res.send(artist);
       } else {
           res.status(404).send({message: 'Not found'});
       }
   } catch (e) {
       res.status(500).send(e);
   }
});

router.delete('/:id',  auth, permit('admin'),async(req, res) => {
   try {
       const artist = await Artists.findByIdAndDelete(req.params.id);

       if (artist) {
           res.send(`Artist by ${req.params.id} was deleted successful`);
       } else {
           res.status(404).send({message: 'Artist not found'});
       }
   } catch (e) {
       res.status(500).send(e);
   }
});

module.exports = router;
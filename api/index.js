require('dotenv').config();
const express = require('express');
const cors = require('cors');
const artists = require('./routers/artists');
const albums = require('./routers/albums');
const tracks = require('./routers/tracks');
const users = require('./routers/users');
const trackHistories = require('./routers/trackHistories');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;

app.use('/artists', artists);
app.use('/albums', albums);
app.use('/tracks', tracks);
app.use('/users', users);
app.use('/trackhistories', trackHistories);

const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(port, () => {
        console.log('Port start on port: ', port);
    });

    exitHook(() => {
        console.log('exiting');
        mongoose.disconnect();
    });
}

run().catch(e => console.log(e));


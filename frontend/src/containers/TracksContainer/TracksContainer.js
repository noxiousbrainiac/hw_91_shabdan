import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {clearTrackError, getTracks} from "../../store/actions/tracksActions";
import {CircularProgress} from "@material-ui/core";
import TracksList from "../../components/Lists/TracksList/TracksList";

const TracksContainer = ({location}) => {
    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracksReducer.tracks);
    const loading = useSelector(state => state.tracksReducer.loading);

    useEffect(() => {
        dispatch(getTracks(location.search));
        return () => {
            dispatch(clearTrackError());
        }
    }, [dispatch, location.search]);


    let TrackContainer = (
        <>
            <h2 style={{textAlign: "center"}}>Tracks</h2>
            <h2>Artist: {tracks[0]?.album.artist.title}</h2>
            <h2>Album: {tracks[0]?.album.title}</h2>
            <TracksList tracks={tracks}/>
        </>
    );

    if (loading === true) {
        TrackContainer = (
            <div
                style={{
                    position: "relative",
                    paddingLeft: "50%",
                    paddingTop: "30%"
                }}
            >
                <CircularProgress
                    color="secondary"
                    size={100}
                />
            </div>
        );
    }

    return TrackContainer;
};

export default TracksContainer;
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Typography} from "@material-ui/core";
import {clearAlbumsError, postAlbum} from "../../store/actions/albumsActions";
import AlbumForm from "../../components/Forms/AlbumForm/AlbumForm";
import {getArtists} from "../../store/actions/artistActions";

const NewAlbum = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.albumsReducer.postError);
    const artists = useSelector(state => state.artistReducer.artists);

    const onSubmit = async albumData => {
        await dispatch(postAlbum(albumData));
    };

    useEffect(() => {
        dispatch(getArtists());
        return () => {
            dispatch(clearAlbumsError());
        }
    }, [dispatch])

    return (
        <>
            <Typography variant="h4">New album</Typography>
            <AlbumForm
                onSubmit={onSubmit}
                error={error}
                artists={artists}
            />
        </>
    );
};

export default NewAlbum;
import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import ArtistForm from "../../components/Forms/ArtistForm/ArtistForm";
import {useDispatch} from "react-redux";
import {clearArtistsError, postArtist} from "../../store/actions/artistActions";

const NewArtist = () => {
    const dispatch = useDispatch()

    const onSubmit = async artistData => {
        await dispatch(postArtist(artistData));
    };

    useEffect(() => {
        return () => {
            dispatch(clearArtistsError());
        }
    }, [dispatch]);

    return (
        <>
            <Typography variant="h4">New artist</Typography>
            <ArtistForm
                onSubmit={onSubmit}
            />
        </>
    );
};

export default NewArtist;
import {
    CLEAR_USER_ERROR,
    LOGIN_USER_FAILURE, LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "../actions/usersActions";

const initialState = {
    user: null,
    registerError: null,
    loginError: null,
    loginLoading: false,
    role: ""
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_USER_SUCCESS:
            return {...state, user: action.payload, registerError: null};
        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.payload};
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.payload, loginError: null, loginLoading: false};
        case LOGIN_USER_FAILURE:
            return {...state, loginError: action.payload, loginLoading: false};
        case LOGIN_USER_REQUEST:
            return {...state, loginLoading: true};
        case LOGOUT_USER:
            return {...state, user: null};
        case CLEAR_USER_ERROR:
            return {...state, loginError: null, registerError: null, loginLoading: false};
        default: return state;
    }
};

export default userReducer;
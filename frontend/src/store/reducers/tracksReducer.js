import {
    CLEAR_TRACK_ERROR, DELETE_TRACK_FAILURE, DELETE_TRACK_REQUEST, DELETE_TRACK_SUCCESS,
    GET_TRACKS_FAILURE,
    GET_TRACKS_REQUEST,
    GET_TRACKS_SUCCESS, POST_TRACK_FAILURE,
    POST_TRACK_REQUEST,
    POST_TRACK_SUCCESS, PUBLISH_TRACK_FAILURE, PUBLISH_TRACK_REQUEST, PUBLISH_TRACK_SUCCESS
} from "../actions/tracksActions";

const initialState = {
    tracks: [],
    error: null,
    loading: false,
    postError: null
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_TRACKS_REQUEST:
            return {...state, loading: true};
        case GET_TRACKS_SUCCESS:
            return {...state, loading: false, tracks: action.payload};
        case GET_TRACKS_FAILURE:
            return {...state, loading: false, error: action.payload};
        case POST_TRACK_REQUEST:
            return {...state, loading: true};
        case POST_TRACK_SUCCESS:
            return {...state, loading: false};
        case POST_TRACK_FAILURE:
            return {...state, loading: false, postError: action.payload};
        case CLEAR_TRACK_ERROR:
            return {...state, error: null, postError: null};
        case DELETE_TRACK_REQUEST:
            return {...state, loading: true};
        case DELETE_TRACK_SUCCESS:
            return {...state, loading: false};
        case DELETE_TRACK_FAILURE:
            return {...state, error: action.payload};
        case PUBLISH_TRACK_REQUEST:
            return {...state, loading: true};
        case PUBLISH_TRACK_SUCCESS:
            return {...state, loading: false};
        case PUBLISH_TRACK_FAILURE:
            return {...state, loading: false, error: action.payload};
        default: return state;
    }
};

export default tracksReducer;
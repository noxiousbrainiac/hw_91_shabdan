import {
    GET_TRACK_HISTORY_FAILURE,
    GET_TRACK_HISTORY_REQUEST,
    GET_TRACK_HISTORY_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
    error: null,
    trackHistory: [],
    loading: false
}

const trackHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_TRACK_HISTORY_REQUEST:
            return {...state, loading: true};
        case GET_TRACK_HISTORY_SUCCESS:
            return {...state, trackHistory: action.payload, loading: false};
        case GET_TRACK_HISTORY_FAILURE:
            return {...state, loading: false, error: action.payload};
        default: return state;
    }
}

export default trackHistoryReducer;
import AxiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";
import {toast} from "react-toastify";
import {getArtists} from "./artistActions";

export const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
export const REGISTER_USER_FAILURE = "REGISTER_USER_FAILURE";

export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAILURE = "LOGIN_USER_FAILURE";
export const LOGIN_USER_REQUEST = "LOGIN_USER_REQUEST";

export const LOGOUT_USER = 'LOGOUT_USER';

export const CLEAR_USER_ERROR = "CLEAR_USER_ERROR";

export const registerUserSuccess = (user) => ({type: REGISTER_USER_SUCCESS, payload: user});
export const registerUserFailure = (error) => ({type: REGISTER_USER_FAILURE, payload: error});

export const loginUserSuccess = (user) => ({type: LOGIN_USER_SUCCESS, payload: user});
export const loginUserFailure = (error) => ({type: LOGIN_USER_FAILURE, payload: error});
export const loginUserRequest = () => ({type: LOGIN_USER_REQUEST});

export const clearUserError = () => ({type: CLEAR_USER_ERROR});

export const registerUser = (user) => async (dispatch) => {
    try {
        dispatch(loginUserRequest());
        const {data} = await AxiosApi.post('/users', user);
        dispatch(registerUserSuccess(data));
        dispatch(historyPush('/'));
    } catch (e) {
        if (e.response && e.response.data) {
            dispatch(registerUserFailure(e.response.data));
        } else {
            dispatch(registerUserFailure({message: "No internet connexion"}));
        }
    }
};

export const loginUser = (user) => async (dispatch) => {
    try {
        const {data} = await AxiosApi.post('/users/session', user);
        dispatch(loginUserSuccess(data.user));
        dispatch(historyPush('/'));
        toast.success('Login successful');
    } catch (e) {
        if (e.response && e.response.data) {
            dispatch(loginUserFailure(e.response.data));
        } else {
            dispatch(loginUserFailure({message: "No internet connexion"}));
        }
    }
};

export const logoutUser = () => async (dispatch) => {
    try {
        await AxiosApi.delete('/users/sessions');
        dispatch({type: LOGOUT_USER});
        dispatch(historyPush('/'));
        dispatch(getArtists());
    } catch (e) {
        if (e.response && e.response.data) {
            dispatch(loginUserFailure(e.response.data));
        } else {
            dispatch(loginUserFailure({message: "No internet connexion"}));
        }
    }
};

export const facebookLogin = data => async (dispatch) => {
    try {
        const response = await AxiosApi.post('/users/facebookLogin', data);
        dispatch(loginUserSuccess(response.data.user));
        dispatch(historyPush('/'));
        toast.success('Login successful');
    } catch (error) {
        toast.error(error.response.data.global);
        dispatch(loginUserFailure(error.response.data));
    }
};
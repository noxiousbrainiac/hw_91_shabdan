import React, {useState} from 'react';
import {Grid, makeStyles, TextField} from "@material-ui/core";
import FormElement from "../../UI/Form/FormElement";
import ButtonWithProgress from "../../UI/ButtonWithProgress/ButtonWithProgress";
import {useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const AlbumForm = ({onSubmit, artists, error}) => {
    const classes = useStyles();
    const loading = useSelector(state1 => state1.albumsReducer.loading);

    const [state, setState] = useState({
        title: "",
        image: null,
        productionYear: "",
        artists: ""
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    }

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <FormElement
                select
                options={artists}
                label="Artists"
                name="artist"
                value={state.artists}
                onChange={inputChangeHandler}
                error={getFieldError('artist')}
            />

            <FormElement
                label="Title"
                name="title"
                value={state.title}
                onChange={inputChangeHandler}
                error={getFieldError('title')}
            />

            <FormElement
                label="Production year"
                name="productionYear"
                value={state.productionYear}
                onChange={inputChangeHandler}
                error={getFieldError('productionYear')}
            />

            <Grid item xs>
                <TextField
                    type="file"
                    name="image"
                    onChange={fileChangeHandler}
                />
            </Grid>
            <Grid item xs>
                <ButtonWithProgress
                    type="submit"
                    variant="contained"
                    color="primary"
                    loading={loading}
                    disabled={loading}
                >
                    Create
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default AlbumForm;
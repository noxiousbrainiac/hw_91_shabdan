import React from 'react';
import HistoryItem from "./HIstoryItem/HistoryItem";
import {Grid} from "@material-ui/core";

const HistoryList = ({trackHistory}) => {
    return (
        <Grid container spacing={2}>
            {trackHistory.length !==0 ? trackHistory.map(item => (
                <HistoryItem
                    key={item._id}
                    artist={item.track.album.artist.title}
                    track={item.track.title}
                    date={item.datetime}
                />
            )): <h1>No data here</h1>}
        </Grid>
    );
};

export default HistoryList;
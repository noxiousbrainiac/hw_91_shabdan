import React from 'react';
import {Button, Card, CardHeader, Grid, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {addTrackToHistory, deleteTrack, publishTrack} from "../../../../store/actions/tracksActions";
import {useLocation} from "react-router-dom";
import ButtonWithProgress from "../../../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles({
    card: {
        height: '100%',
        cursor: "pointer",
        padding: "10px"
    }
});

const TrackItem = ({title, number, duration, id, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const location = useLocation();
    const loading = useSelector(state => state.tracksReducer.loading);

    const add = () => {
        if (user) {
            dispatch(addTrackToHistory(id));
        }
    }

    const remove = () => {
        dispatch(deleteTrack(id, location.search));
    };

    const publish = () => {
        dispatch(publishTrack(id, location.search));
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card
                className={classes.card}
            >
                <CardHeader title={`Track: ${title}`}/>
                {user?.role === 'admin' ?
                    <Grid container spacing={2} style={{padding: "10px"}}>
                        <Grid item>
                            <Button
                                variant="contained"
                                color={"primary"}
                                onClick={add}
                            >
                                Listen
                            </Button>
                        </Grid>
                        <Grid item>
                            <ButtonWithProgress
                                onClick={remove}
                                variant="contained"
                                color="secondary"
                                loading={loading}
                                disabled={loading}
                            >
                                Remove
                            </ButtonWithProgress>
                        </Grid>
                        <Grid item>
                            <ButtonWithProgress
                                onClick={publish}
                                fullWidth
                                variant="contained"
                                color={published === false  ? "primary" : "secondary"}
                                loading={loading}
                                disabled={loading}
                            >
                                {published === false ? "Publish" : "Unpublish"}
                            </ButtonWithProgress>
                        </Grid>
                    </Grid>
                    : null
                }
                <h4>Track number: {number}</h4>
                <span>Duration: <b>{duration}</b></span>
            </Card>
        </Grid>
    );
};

export default TrackItem;
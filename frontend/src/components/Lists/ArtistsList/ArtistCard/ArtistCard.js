import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, Grid, IconButton, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteArtist, publishArtist} from "../../../../store/actions/artistActions";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import ButtonWithProgress from "../../../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles({
    card: {
        height: '100%',
        cursor: "pointer"
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const ArtistCard = ({photo, name, id, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const loading = useSelector(state => state.artistReducer.loading);

    const remove = () => {
        dispatch(deleteArtist(id));
    }

    const publish = async () => {
        await dispatch(publishArtist(id));
    }

    let cardImage = "https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/9/2/8/5/235829-6-eng-GB/Feed-Test-SIC-Feed-20142_news_large.jpg";

    if (photo) {
        cardImage = "http://localhost:8000/" + photo;
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card
                className={classes.card}
            >
                <CardHeader
                    title={name}
                />
                {user?.role === 'admin' ?
                    <Grid container spacing={2} style={{padding: "10px"}}>
                        <Grid item>
                            <ButtonWithProgress
                                onClick={remove}
                                variant="contained"
                                color="secondary"
                                loading={loading}
                                disabled={loading}
                            >
                                Remove
                            </ButtonWithProgress>
                        </Grid>
                        <Grid item>
                            <ButtonWithProgress
                                onClick={publish}
                                fullWidth
                                variant="contained"
                                color={published === false  ? "primary" : "secondary"}
                                loading={loading}
                                disabled={loading}
                            >
                                {published === false ? "Publish" : "Unpublish"}
                            </ButtonWithProgress>
                        </Grid>
                    </Grid>
                     : null
                }
                <CardActions>
                    <IconButton component={Link} to={`/albums?artist=${id}`}>
                        <ArrowForwardIcon />
                    </IconButton>
                </CardActions>
                <CardMedia
                    image={cardImage}
                    className={classes.media}
                />
            </Card>
        </Grid>
    );
};

export default ArtistCard;
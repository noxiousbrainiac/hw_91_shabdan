import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, Grid, IconButton, makeStyles} from "@material-ui/core";
import {Link, useLocation} from "react-router-dom";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {useDispatch, useSelector} from "react-redux";
import {deleteAlbum, publishAlbum} from "../../../../store/actions/albumsActions";
import ButtonWithProgress from "../../../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles({
    card: {
        height: '100%',
        cursor: "pointer",
        textAlign: "center"
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});

const AlbumCard = ({photo, title, year, id, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const location = useLocation();
    const loading = useSelector(state => state.albumsReducer.loading);

    let cardImage = "https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/9/2/8/5/235829-6-eng-GB/Feed-Test-SIC-Feed-20142_news_large.jpg";

    if (photo) {
        cardImage = "http://localhost:8000/" + photo;
    }

    const remove = () => {
        dispatch(deleteAlbum(id, location.search));
        console.log(location);
    }

    const publish =  () => {
        dispatch(publishAlbum(id, location.search));
    }

    return (
        <>
            <Grid item xs={12} sm={6} md={6} lg={4}>
                <Card
                    className={classes.card}
                >
                    <CardHeader title={title}/>
                    {user?.role === 'admin' ?
                        <Grid container spacing={2} style={{padding: "10px"}}>
                            <Grid item>
                                <ButtonWithProgress
                                    onClick={remove}
                                    variant="contained"
                                    color="secondary"
                                    loading={loading}
                                    disabled={loading}
                                >
                                    Remove
                                </ButtonWithProgress>
                            </Grid>
                            <Grid item>
                                <ButtonWithProgress
                                    onClick={publish}
                                    fullWidth
                                    variant="contained"
                                    color={published === false  ? "primary" : "secondary"}
                                    loading={loading}
                                    disabled={loading}
                                >
                                    {published === false ? "Publish" : "Unpublish"}
                                </ButtonWithProgress>
                            </Grid>
                        </Grid>
                        : null
                    }
                    <CardActions>
                        <IconButton component={Link} to={`/tracks?album=${id}`}>
                            <ArrowForwardIcon />
                        </IconButton>
                    </CardActions>
                    <span><b>Production year:</b> {year}</span>
                    <CardMedia
                        image={cardImage}
                        className={classes.media}
                    />
                </Card>
            </Grid>
        </>
    );
};

export default AlbumCard;
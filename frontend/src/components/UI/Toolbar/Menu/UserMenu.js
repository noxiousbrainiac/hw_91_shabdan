import React, {useState} from 'react';
import {Avatar, Button, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {historyPush} from "../../../../store/actions/historyActions";
import {logoutUser} from "../../../../store/actions/usersActions";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
    menuColor: {
        color: "beige"
    }
})

const UserMenu = ({user}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const checkUploadsStr = ['u', 'p', 'l', 'o', 'a', 'd', 's'];
    const array = [];

    let cardImage ="";

    for (let i = 0; i < checkUploadsStr.length; i++) {
        if  (user.avatarImage[i] === checkUploadsStr[i]) {
            array.push(user.avatarImage[i]);
        }
    }

    if (array.join('') === checkUploadsStr.join('')) {
        cardImage = "http://localhost:8000/" + user?.avatarImage;
    } else {
        cardImage = user?.avatarImage;
    }

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const toTrackHistory = (event) => {
        setAnchorEl(event.currentTarget);
        dispatch(historyPush('/trackhistory'));
    }

    const toLogOut = () => {
        dispatch(logoutUser());
    }

    return (
        <>
            <Button
                className={classes.menuColor}
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
                startIcon={<Avatar src={cardImage}/>}
            >
                Hello, {user.displayName}!
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={toTrackHistory}>Track History</MenuItem>
                {user?.role === "admin" ?
                    <span>
                        <MenuItem onClick={handleClose} component={Link} to={'/artists/new'}>Add artist</MenuItem>
                        <MenuItem onClick={handleClose} component={Link} to={'/albums/new'}>Add album</MenuItem>
                        <MenuItem onClick={handleClose} component={Link} to={'/tracks/new'}>Add track</MenuItem>
                    </span>  : null
                }
                <MenuItem onClick={toLogOut}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;
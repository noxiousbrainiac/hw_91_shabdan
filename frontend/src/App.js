import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import ArtistsAlbumsContainer from "./containers/ArtistsAlbumsContainer/ArtistsAlbumsContainer";
import TracksContainer from "./containers/TracksContainer/TracksContainer";
import Register from "./containers/Register/Register";
import Layout from "./components/UI/Layout/Layout";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import {useSelector} from "react-redux";
import NewArtist from "./containers/NewArtist/NewArtist";
import NewAlbum from "./containers/NewAlbum/NewAlbum";
import NewTrack from "./containers/NewTrack/NewTrack";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route path='/albums' exact component={ArtistsAlbumsContainer}/>
                <ProtectedRoute
                    path="/albums/new"
                    component={NewAlbum}
                    isAllowed={user?.role === 'admin'}
                    redirectTo="/login"
                />
                <Route path='/tracks' exact component={TracksContainer}/>
                <ProtectedRoute
                    path="/tracks/new"
                    component={NewTrack}
                    isAllowed={user?.role === 'admin'}
                    redirectTo="/login"
                />
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path='/trackhistory' component={TrackHistory}/>
                <ProtectedRoute
                    path="/artists/new"
                    component={NewArtist}
                    isAllowed={user?.role === 'admin'}
                    redirectTo="/login"
                />
            </Switch>
        </Layout>
    );
};

export default App;